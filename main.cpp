#include <iostream>
#include <cmath>
#include <string>

#include "header.h"
#include "LeSportif.h"
#include "Soccer.h"

using namespace std;

int main() {

    string name;
    cout <<"Bonjour! Comment vous vous appelez?" << endl;
    getline(cin, name);
    jumpLine();
    cout <<"Et bien " << name << ", que voulez-vous tester ?" << endl;
    jumpLine();

    int choice;
    int leave;
    int counter = -1;

    do{
        do{
            menuTest();
            jumpLine();
            cin >> choice;
            jumpLine();

            switch(choice){
                case 1:
                    slipTen();
                    break;
                case 2:
                    testLogique();
                    break;
                case 3:
                    {LeSportif sportifUn(105, "Sena", "Kobayakawa", 400);
                    LeSportif sportifDeux(106, "Yoichi", "Hiruma");

                    sportifUn.afficher();
                    sportifUn.setNbTweet(5000);
                    cout << "Nombre de tweet du 1eme sportif avec changement de 5000 : " << sportifUn.getNbTweet() << endl;

                    if(sportifUn.twitterFidele(sportifUn) == true) cout << "Le sportif est un membre fidele" << endl;
                    else cout << "Le sportif n'est pas un membre fidele de twitter, il faut avoir 100.000 tweets minimum." << endl;
                    jumpLine();
                    cout << "Nombre de tweet du 1eme sportif avec ajout de 110000 : " << sportifUn.ajouterTweet(1100000) << endl;
                    sportifUn.setNbTweet(sportifUn.ajouterTweet(1100000));
                    if(sportifUn.twitterFidele(sportifUn) == true) cout << "Le sportif est un membre fidele" << endl;
                    else cout << "Le sportif n'est pas un membre fidele de twitter, il faut avoir 100.000 tweets minimum." << endl;
                    cout << " ---------------- " << endl;

                    cout << "Numero du 2eme sportif : " << sportifDeux.getNumero() << endl;
                    string nomSportifDeux = sportifDeux.getNom();
                    cout << "Nom du 2eme sportif : " << nomSportifDeux << endl;
                    cout << "Prenom du 2eme sportif : " << sportifDeux.getPrenom() << endl;
                    sportifDeux.setNumero(100);
                    cout << "Changement de numero du 2eme sportif : " << sportifDeux.getNumero() << endl;}
                    cout << " ---------------- " << endl;

                    /*Soccer soccerUn;
                    soccerUn.afficheSoccer();
                    soccerUn.setNom("Alors");
                    cout << soccerUn.getNom() << endl;*/
                    jumpLine();
                    break;
                case 4:
                    alphaber();
                    break;
                case 5:
                    cout << "Voulez-vous vraiment quitter l'application ? (Oui = 1 | Non = 0)" << endl;
                    break;
                default:
                    cout << "Entre 1 et 5 par contre..." << endl;
                    counter--;
                    jumpLine();
                    break;
            }
        counter++;
        }while(choice !=5);
        cin >> leave;
        leave ? cout << "Vous avez fait " << counter << " test. " << "\n" << "Au revoir " << name << " !" << endl : cout << "C'est reparti !" << endl;
        cout << " " << endl;
    }while (leave == 0);
}


