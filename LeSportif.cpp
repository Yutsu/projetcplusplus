#include <iostream>
#include <string>
#include "LeSportif.h"

using namespace std;

LeSportif::LeSportif(int numero, string nom, string prenom, int nbtweet){
    this->numero = numero;
    this->nom = nom;
    this->prenom = prenom;
    this->nbtweet = nbtweet;
}

LeSportif::LeSportif(int numero, string nom, string prenom){
    this->numero = numero;
    this->nom = nom;
    this->prenom = prenom;
}

//LeSportif::LeSportif();


int LeSportif::getNumero(){
    return this->numero;
}

void LeSportif::setNumero(int numero){
    this->numero = numero;
}

string LeSportif::getNom(){
    return this->nom;
}

void LeSportif::setNom(string nom){
    this->nom = nom;
}

string LeSportif::getPrenom(){
    return this->prenom;
}

void LeSportif::setPrenom(string prenom){
    this->prenom = prenom;
}

int LeSportif::getNbTweet(){
    return this->nbtweet;
}

void LeSportif::setNbTweet(int nbtweet){
    this->nbtweet = nbtweet;
}

int LeSportif::ajouterTweet(int ajout){
    return this->nbtweet + ajout;
}

bool LeSportif::twitterFidele(LeSportif sportif){
    if(sportif.getNbTweet() > 100000){
        return true;
    }
	else{
		return false;
	}
}

void LeSportif::afficher(){
    cout <<"Le sportif numero " << this->numero << " s'appelle " << this->nom
    << " " << this->prenom << " et a tweet " << this->nbtweet << " fois." << endl;
}
