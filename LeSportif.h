#ifndef LESPORTIF_H_INCLUDED
#define LESPORTIF_H_INCLUDED
#include <string>
#include <iostream>

using namespace std;

class LeSportif{

    private:
    int numero;
    string nom;
    string prenom;
    int nbtweet;

    public:
    LeSportif();
    LeSportif(int numero, string nom, string prenom, int nbtweet);
    LeSportif(int numero, string nom, string prenom);

    int getNumero();
    void setNumero(int numero);
    string getNom();
    void setNom(string nom);
    string getPrenom();
    void setPrenom(string prenom);
    int getNbTweet();
    void setNbTweet(int nbtweet);

    bool twitterFidele(LeSportif unSportif);
    int ajouterTweet(int ajout);
    virtual void afficher();
};

#endif // LESPORTIF_H_INCLUDED
